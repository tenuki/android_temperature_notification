package ar.com.endlesstruction.onst3;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Display{
    public static void show(Context context, String temperature) {
        show_res(context, temperature);
    }

     static String temp_to_resource_name(String temp) {
        temp = temp.replace("-", "_").replace(".", "_");
        return "base_"+temp;
    }

    static int
    getMyRes(Context ctx, String temperature) {
        Resources res = ctx.getResources();
        String name = temp_to_resource_name(temperature);
        return res.getIdentifier(name,"drawable", ctx.getPackageName());
    }

    static void
    show_res(Context ctx, String data) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ctx)
                        .setSmallIcon( getMyRes(ctx, data))
                        .setContentTitle(data+"C")
                        .setNumber(17)
                        .setContentText(data+"C");

        Intent resultIntent = new Intent(ctx, AppCompatPreferenceActivity.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        ctx, 0, resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        int mNotificationId = 001; // Sets an ID for the notification
        NotificationManager mNotifyMgr = // Gets an instance of the NotificationManager service
                (NotificationManager) ctx.getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build()); // Builds the notification and issues it.
    }
}
