package ar.com.endlesstruction.onst3;

/**
 * Created by aweil on 4/28/2017.
 */


public final class AppConstants {

    // Defines a custom Intent action
    public static final String BROADCAST_ACTION =
            "ar.com.endlesstruction.onst3.BROADCAST";

    // Defines the key for the status "extra" in an Intent
    public static final String EXTENDED_DATA_STATUS =
            "ar.com.endlesstruction.onst3.STATUS";
}
