package ar.com.endlesstruction.onst3;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Fetch {
    static public String fetch() {
        URL url = null;
        try {
            url = new URL("http://weather.endlesstruction.com.ar/current.txt");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "error1";
        }
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
            return "error2";
        }
        try {
            InputStream in = null;
            try {
                in = new BufferedInputStream(urlConnection.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                return "error3";
            }
            return readStream(in);
        } finally {
            urlConnection.disconnect();
        }
    }

    private static String readStream(InputStream in) {
        InputStreamReader isReader = new InputStreamReader(in);
        BufferedReader reader = new BufferedReader(isReader);
        try {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return "error4";
        }
    }
}
