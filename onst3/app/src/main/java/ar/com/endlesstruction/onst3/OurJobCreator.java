package ar.com.endlesstruction.onst3;

import android.content.Context;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;


public class OurJobCreator implements JobCreator {
    private final Context m_ctx;

    public OurJobCreator(Context ctx) {
        m_ctx = ctx;
    }

    @Override
    public Job create(String tag) {
        switch (tag) {
            case MyJob.TAG:
                return new MyJob(m_ctx);
            default:
                return null;
        }
    }
}