package ar.com.endlesstruction.onst3;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


public class UpdateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String temperature = intent.getExtras().getString(AppConstants.EXTENDED_DATA_STATUS);
        Display.show(context.getApplicationContext(), temperature);
    }

    public static void broadcast_temperature(Context ctx, String temperature) {
        // broadcast result...
        Intent localIntent =
                new Intent(AppConstants.BROADCAST_ACTION)
                        // Puts the status into the Intent
                        .putExtra(AppConstants.EXTENDED_DATA_STATUS, temperature);
        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(ctx).sendBroadcast(localIntent);
    }
}
