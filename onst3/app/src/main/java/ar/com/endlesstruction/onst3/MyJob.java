package ar.com.endlesstruction.onst3;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

public class MyJob extends Job {
    public static final String TAG = "job_demo_tag";
    private final Context m_ctx;

    public MyJob(Context ctx) {
        m_ctx = ctx;
    }

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        String temp = Fetch.fetch();
        if (temp=="")
            return Result.FAILURE;
        UpdateReceiver.broadcast_temperature(m_ctx, temp);
        return Result.SUCCESS;
    }

    public static void scheduleJob() {
        new JobRequest.Builder(MyJob.TAG)
                .setPeriodic(900*1000, 300*1000)
                .setUpdateCurrent(true)
                .setPersisted(true)
                .build()
                .schedule();
    }
}
